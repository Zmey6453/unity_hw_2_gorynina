﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game1
{

[RequireComponent(typeof(CharacterController))]
public class Unit : MonoBehaviour
{
  readonly Vector3 GRAVITY = new Vector3(0.0f, -9.8f, 0.0f);

  public Vector3 speed;

  CharacterController cc;

  void Start()
  {
    cc = GetComponent<CharacterController>();
  }

  void Update()
  {
    Vector3 final_speed = speed;
    if(!cc.isGrounded)
      final_speed += GRAVITY;
    cc.Move(final_speed * Time.deltaTime);
  }

  public void Reset()
  {
    speed = Vector3.zero;
    transform.position = Vector3.zero;
  }

  void OnTriggerEnter(Collider col)
  {
    Debug.Log(col.gameObject.tag);

    var coin = col.gameObject.GetComponent<Coin>();
    if(coin != null)
    {
      coin.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), 0.0f, Random.Range(-5.0f, 5.0f));
      return;
    }

    if(col.gameObject.tag == "Enemy")
    {
      Game.self.Reset();
      return;
    }
  }
}

}
