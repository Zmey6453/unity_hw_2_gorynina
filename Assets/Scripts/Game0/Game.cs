﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game0
{

    public class Game : MonoBehaviour
    {
        public GameObject target_spawn_placeholder;
        public Hero hero;
        public Target origin;
        public Target true_origin;
        public float start_hero_speed = 0.2f;
        public float step_hero_speed = 0.01f;
        public float max_hero_speed = 1.0f;
        public float distance_between_targets = 1.2f;
        float start_game_time;
        public float game_time = 30;
        public Text lefttime_ui;
        public GameObject timer;
        public float timerScale;
        public float timerTick;

        List<Target> targets = new List<Target>();

        int target_count = 1;

        public float laser_time = 1.0f;
        float current_laser_time = 0.0f;

        enum GameState
        {
            GAME,
            FIRE,
            NEXT_LEVEL,
            RESET
        }

        GameState state = GameState.GAME;
        GameState pending_state;

        void Start()
        {
            SetupGame();
        }

        void SetupGame(bool win = true)
        {
            if (!win)
                target_count = 1; //Reset game
            SpawnTargets();
            SetupHeroMovement();
            RandomizeTrueTargetPosition();
            state = GameState.GAME;
            SetTimer();
        }

        void SetTimer()
        {
            start_game_time = Time.time;
            lefttime_ui = FindObjectOfType<Text>();
            game_time = hero.speed * 20;

            float timerPosition = (distance_between_targets * targets.Count) / 2.0f;
            timer.transform.position = new Vector3(0, -0.5f, 4.5f);
            timerScale = distance_between_targets * targets.Count;
            timerTick =  game_time/timerScale;

            timer.transform.localScale = new Vector3(timerScale, 0.01f, timerScale);


        }
        void Timer()
        {
            if (timer.transform.localScale.x > 0)
                timer.transform.localScale = new Vector3(timer.transform.localScale.x - timerTick * 0.002f, 0.01f, timer.transform.localScale.x - timerTick * 0.002f);
        }

        float GetLeftTime()
        {
            return Mathf.Max(0, start_game_time + game_time - Time.time);
        }

        void SetupHeroMovement()
        {
            hero.x_a = targets[0].transform.position.x;
            hero.x_b = targets[targets.Count - 1].transform.position.x;
            hero.speed = start_hero_speed + step_hero_speed * (targets.Count - 1);
            hero.speed = Mathf.Min(hero.speed, max_hero_speed);
        }

        void SpawnTargets()
        {
            foreach (var target in targets)
                GameObject.Destroy(target.gameObject);

            targets.Clear();

            SpawnTarget(true_origin);

            for (int i = 1; i < target_count; ++i)
                SpawnTarget(origin);

            var start_position_x = target_spawn_placeholder.transform.position.x + 0.6f -
              (distance_between_targets * target_count) / 2.0f;

            for (int i = 0; i < targets.Count && target_count > 1; ++i)
            {
                targets[i].transform.position = new Vector3(
                  start_position_x + distance_between_targets * (float)i,
                  target_spawn_placeholder.transform.position.y,
                  target_spawn_placeholder.transform.position.z);
            }

            target_count++;
        }

        void RandomizeTrueTargetPosition()
        {
            int index = Random.Range(0, targets.Count);
            var true_target_position = targets[0].transform.position;
            targets[0].transform.position = targets[index].transform.position;
            targets[index].transform.position = true_target_position;
        }

        void SpawnTarget(Target origin)
        {
            var new_target = Target.Instantiate(origin);
            new_target.transform.position = target_spawn_placeholder.transform.position;
            targets.Add(new_target);
        }

        void ProcessInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                state = GameState.FIRE;
                foreach (var target in targets)
                {
                    if (!target.is_true_target)
                        continue;

                    if (Mathf.Abs(hero.transform.position.x - target.transform.position.x) < 1.0f)
                    {
                        pending_state = GameState.NEXT_LEVEL;
                        return;
                    }
                }

                pending_state = GameState.RESET;
            }
        }

        void Update()
        {
            switch (state)
            {
                case GameState.GAME:
                    ProcessInput();
                    hero.Tick(Time.time);
                    lefttime_ui.text = "Left Time: " + Mathf.Round(GetLeftTime());
                    Timer();
                    if (GetLeftTime() <= 0) state = GameState.RESET;
                    break;
                case GameState.FIRE:
                    if (current_laser_time == 0.0f)
                        hero.SetLaserState(true);
                    current_laser_time += Time.deltaTime;
                    if (current_laser_time >= laser_time)
                    {
                        state = pending_state;
                        current_laser_time = 0.0f;
                        hero.SetLaserState(false);
                    }
                    break;
                case GameState.NEXT_LEVEL:
                    SetupGame();
                    break;
                case GameState.RESET:
                    SetupGame(win: false);
                    break;
            }
        }
    }
}