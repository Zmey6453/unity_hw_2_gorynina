using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game0
{

public class Hero : MonoBehaviour
{
  public float x_a;
  public float x_b;

  public float speed = 1.0f;

  public GameObject laser;

  public void SetLaserState(bool state)
  {
    laser.SetActive(state);
  }

  void Start()
  {
    SetLaserState(false);
  }

  public void Tick(float time)
  {
    transform.position = new Vector3(Mathf.Lerp(x_a, x_b, Mathf.PingPong(time * speed, 1.0f)), 0.0f, 0.0f);
  }
}

}
