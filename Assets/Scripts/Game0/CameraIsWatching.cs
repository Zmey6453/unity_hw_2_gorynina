﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game0
{
    public class CameraIsWatching : MonoBehaviour
    {
        public Transform target;
        public float time;
        //public float start_hero_speed = 0.2f;
        Vector3 offset = new Vector3(0, 6, -7);
        private Vector3 velocity = Vector3.zero;

        void Update()
        {
            Vector3 targetPosition = target.transform.position + offset;
            time = 1.0f - GameObject.FindWithTag("Game").GetComponent<Game>().hero.speed;
            

            // transform.position = Vector3.Lerp(transform.position, targetPosition, smoothTime);

            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, time);

        }

    }
}
